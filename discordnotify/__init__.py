"""Forward Alliance Auth notifications to users on Discord."""

# pylint: disable = invalid-name
default_app_config = "discordnotify.apps.DiscordnotifyConfig"

__title__ = "Discord Notify"
__version__ = "2.0.0"
