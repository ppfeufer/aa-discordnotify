# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased] - yyyy-mm-dd

## [2.0.0] - 2024-05-10

### Added

- New setting `DISCORDPROXY_HOST` to configure a different host address for discordproxy, which is needed e.g. for the new AA4 docker setup.

### Changed

- BREAKING CHANGE: Setting `DISCORDNOTIFY_DISCORDPROXY_PORT` renamed to `DISCORDPROXY_PORT`. The default is unchanged, so if you did not change this setting you do not have to do anything.

## [1.3.0] - 2023-12-27

### Changed

- `DISCORDNOTIFY_MARK_AS_VIEWED` is now set to `True` by default
- Added support for AA4

## [1.2.1] - 2023-10-06

### Changed

- Added pylint checks
- Refactoring

### Fixed

- Wrong messages / messages_plus import

## [1.2.0] - 2023-07-14

### Added

- Added support for Python 3.10
- Added support for Python 3.11

### Changed

- Log message about missing Discord user promoted to warning
- Dropped support for Python 3.7
- Migrated to AA 3 and dropped support for AA 2
- Migrated build process to PEP 621
- Updated pre-commit

## [1.1.1] - 2022-06-17

### Changed

- Add PyPI wheel

## [1.1.0] - 2021-12-29

### Changed

- Now shows app logo and name in footer
- Update CI for AA 2.9+ / Django 3.2
- Removed support for Python 3.6
- Replace native gRPC client with DiscordClient
- Now retries notification tasks if they reach timeout

## [1.0.1] - 2021-05-24

### Changed

- Truncate large notifications instead of splitting them into multiple messages on Discord

## [1.0.0] - 2021-05-24

### Added

- Proper logging of discordproxy API errors

### Fixed

- Long notifications throw an error (#3)

## [0.1.0] - 2021-04-17

### Added

- Initial release

### Changed

### Fixed
